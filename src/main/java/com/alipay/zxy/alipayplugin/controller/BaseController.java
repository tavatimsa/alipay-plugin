package com.alipay.zxy.alipayplugin.controller;

import lombok.extern.slf4j.Slf4j;

/**
 * 控制器基类
 *
 * @author zxy
 * @date 2019-06-28
 */
@Slf4j
class BaseController {
    void getErrorResult(Exception e) {
        if (e.getCause() instanceof java.security.spec.InvalidKeySpecException) {
            log.error("商户私钥格式不正确，请确认配置文件alipay-config.properties中是否配置正确", e);
        } else {
            log.error("其他错误", e);
        }
    }
}
