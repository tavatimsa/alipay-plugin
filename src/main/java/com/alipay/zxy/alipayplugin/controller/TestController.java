package com.alipay.zxy.alipayplugin.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.*;
import com.alipay.zxy.alipayplugin.message.request.*;
import com.alipay.zxy.alipayplugin.message.response.*;
import com.alipay.zxy.alipayplugin.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

/**
 * 控制器示例(演示只传入基本必要参数，如有其他需求请自行阅读支付宝api文档)
 *
 * @author zxy
 * @date 2019-06-28
 */
@Slf4j
@Api(value = "控制器示例", tags = "控制器示例")
@RestController
public class TestController extends BaseController {

    private final AlipayTradePayService alipayTradePayService;
    private final AlipayTradeRefundService alipayTradeRefundService;
    private final AlipayUserAgreementPageSignService alipayUserAgreementPageSignService;
    private final AlipayUserAgreementUnSignService alipayUserAgreementUnSignService;
    private final AlipayUserAgreementQueryService alipayUserAgreementQueryService;
    private final AlipayTradePayQueryService tradePayQueryService;

    public TestController(AlipayTradePayService alipayTradePayService, AlipayTradeRefundService alipayTradeRefundService, AlipayUserAgreementPageSignService alipayUserAgreementPageSignService, AlipayUserAgreementUnSignService alipayUserAgreementUnSignService, AlipayUserAgreementQueryService alipayUserAgreementQueryService, AlipayTradePayQueryService tradePayQueryService) {
        this.alipayTradePayService = alipayTradePayService;
        this.alipayTradeRefundService = alipayTradeRefundService;
        this.alipayUserAgreementPageSignService = alipayUserAgreementPageSignService;
        this.alipayUserAgreementUnSignService = alipayUserAgreementUnSignService;
        this.alipayUserAgreementQueryService = alipayUserAgreementQueryService;
        this.tradePayQueryService = tradePayQueryService;
    }

    @ApiOperation(value = "支付宝代扣扣款")
    @RequestMapping(value = "/deduction")
    public Object deduction(AlipayTradePayReq alipayReq) {
        try {
            AlipayTradePayModel alipayModel = new AlipayTradePayModel();
            AgreementParams agreementParams = new AgreementParams();
            agreementParams.setAgreementNo(alipayReq.getAgreementNo());
            alipayModel.setAgreementParams(agreementParams);
            alipayModel.setSubject(alipayReq.getSubject());
            alipayModel.setTotalAmount(alipayReq.getTotalAmount());
            alipayModel.setOutTradeNo(alipayReq.getOutTradeNo());
            AlipayTradePayDTO tradePayDTO = alipayTradePayService.pay(alipayModel);
            // TODO 根据业务需求自行添加
        } catch (AlipayApiException e) {
            getErrorResult(e);
        }
        return null;
    }

    @ApiOperation(value = "支付宝退款")
    @RequestMapping(value = "/refund")
    public Object refund(AlipayTradeRefundReq alipayReq) {
        try {
            AlipayTradeRefundModel alipayModel = new AlipayTradeRefundModel();
            alipayModel.setTradeNo(alipayReq.getTradeNo());
            alipayModel.setRefundAmount(alipayReq.getRefundAmount());
            AlipayTradeRefundDTO tradePayDTO = alipayTradeRefundService.refund(alipayModel);
            // TODO 根据业务需求自行添加
        } catch (AlipayApiException e) {
            getErrorResult(e);
        }
        return null;
    }

    @ApiOperation(value = "支付宝签约")
    @RequestMapping(value = "/pageSign")
    public Object pageSign(AlipayPageSignReq alipayReq) {
        try {
            AlipayUserAgreementPageSignModel alipayModel = new AlipayUserAgreementPageSignModel();
            alipayModel.setExternalLogonId(alipayReq.getExternalLogonId());
            String returnUrl = alipayReq.getReturnUrl();
            AlipayUserAgreementSignDTO agreementSignDTO = alipayUserAgreementPageSignService.agreement(alipayModel, returnUrl);
            // TODO 根据业务需求自行添加
        } catch (AlipayApiException e) {
            getErrorResult(e);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @ApiOperation(value = "支付宝解约")
    @RequestMapping(value = "/unSign")
    public Object unSign(AlipayUnSignReq alipayReq) {
        try {
            AlipayUserAgreementUnsignModel alipayModel = new AlipayUserAgreementUnsignModel();
            alipayModel.setAgreementNo(alipayReq.getAgreementNo());
            AlipayUnSignDTO alipayUnSignDTO = alipayUserAgreementUnSignService.unSign(alipayModel);
            // TODO 根据业务需求自行添加
        } catch (AlipayApiException e) {
            getErrorResult(e);
        }
        return null;
    }

    @ApiOperation(value = "支付宝用户授权签约查询")
    @RequestMapping(value = "/querySign")
    public Object querySign(AlipayQuerySignReq alipayReq) {
        try {
            AlipayUserAgreementQueryModel alipayModel = new AlipayUserAgreementQueryModel();
            alipayModel.setAgreementNo(alipayReq.getAgreementNo());
            AlipayUserAgreementQueryDTO agreementQueryDTO = alipayUserAgreementQueryService.query(alipayModel);
            // TODO 根据业务需求自行添加
        } catch (AlipayApiException e) {
            getErrorResult(e);
        }
        return null;
    }

    @ApiOperation(value = "支付宝用户交易查询")
    @RequestMapping(value = "/queryPay")
    public Object queryPay(AlipayTradeQueryReq alipayReq) {
        try {
            AlipayTradeQueryModel alipayModel = new AlipayTradeQueryModel();
            if (StringUtils.isNotBlank(alipayReq.getTradeNo())) {
                alipayModel.setTradeNo(alipayReq.getTradeNo());
            } else if (StringUtils.isNotBlank(alipayReq.getOutTradeNo())) {
                alipayModel.setOutTradeNo(alipayReq.getOutTradeNo());
            } else {
                log.error("缺少必要参数");
                return null;
            }
            AlipayTradeQueryDTO alipayTradeQueryDTO = tradePayQueryService.query(alipayModel);
            // TODO 根据业务需求自行添加
        } catch (AlipayApiException e) {
            getErrorResult(e);
        }
        return null;
    }
}
