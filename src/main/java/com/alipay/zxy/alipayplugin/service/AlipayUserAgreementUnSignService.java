package com.alipay.zxy.alipayplugin.service;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayUserAgreementUnsignModel;
import com.alipay.api.request.AlipayUserAgreementUnsignRequest;
import com.alipay.api.response.AlipayUserAgreementUnsignResponse;
import com.alipay.zxy.alipayplugin.config.AlipayConfig;
import com.alipay.zxy.alipayplugin.config.DefaultAlipayClientFactory;
import com.alipay.zxy.alipayplugin.message.response.AlipayUnSignDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zxy
 */
@Slf4j
@Service
public class AlipayUserAgreementUnSignService extends BaseService {

    private final AlipayConfig alipayConfig;

    @Autowired
    public AlipayUserAgreementUnSignService(AlipayConfig alipayConfig) {
        this.alipayConfig = alipayConfig;
    }

    public AlipayUnSignDTO unSign(AlipayUserAgreementUnsignModel agreementUnsignModel) throws AlipayApiException {
        //初始化请求类
        AlipayUserAgreementUnsignRequest alipayRequest = new AlipayUserAgreementUnsignRequest();
        alipayRequest = (AlipayUserAgreementUnsignRequest) getAlipayRequest(alipayRequest, alipayConfig, agreementUnsignModel);
        log.info("支付宝请求参数alipayRequest：{}", JSONObject.toJSONString(alipayRequest));
        //sdk请求客户端，已将配置信息初始化
        AlipayClient alipayClient = DefaultAlipayClientFactory.getInstance(alipayConfig);
        //因为是接口服务，使用exexcute方法获取到返回值
        AlipayUserAgreementUnsignResponse alipayResponse = alipayClient.execute(alipayRequest);
        log.info("alipayResponse:{}", JSONObject.toJSONString(alipayResponse));
        if (alipayResponse.isSuccess()) {
            log.info("支付宝个人代扣协议解约接口调用成功");
            AlipayUnSignDTO alipayUnSignDTO = new AlipayUnSignDTO();
            alipayUnSignDTO.setSuccess(true);
            return alipayUnSignDTO;
        } else {
            log.info("支付宝个人代扣协议解约接口调用失败");
            return null;
        }
    }
}
