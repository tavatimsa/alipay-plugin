package com.alipay.zxy.alipayplugin.message.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
@ApiModel(value = "支付宝退款请求model")
public class AlipayTradeRefundReq implements Serializable {
    @ApiModelProperty(value = "支付宝交易订单号", example = "2019042822001468691031411111")
    private String tradeNo;
    @ApiModelProperty(value = "退款金额(退款金额必须小于等于支付金额)", example = "1.00")
    private String refundAmount;
}
