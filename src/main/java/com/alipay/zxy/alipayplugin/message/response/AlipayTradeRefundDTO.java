package com.alipay.zxy.alipayplugin.message.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
public class AlipayTradeRefundDTO implements Serializable {
    private String tradeNo;
    private Long gmtRefundPay;
    private String outTradeNo;
    private String refundFee;
    private String sendBackFee;
    private String buyerUserId;
    private String buyerLogonId;
}