package com.alipay.zxy.alipayplugin.message.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
public class AlipayTradeQueryDTO implements Serializable {
    private String tradeNo;
    private String outTradeNo;
    /**
     * 交易状态 (WAIT_BUYER_PAY 交易创建，等待买家付款
     * TRADE_CLOSED 未付款交易超时关闭，或支付完成后全额退款
     * TRADE_SUCCESS 交易支付成功
     * TRADE_FINISHED 交易结束，不可退款)
     */
    private String tradeStatus;
    private String totalAmount;
    private Long sendPayDate;
}
