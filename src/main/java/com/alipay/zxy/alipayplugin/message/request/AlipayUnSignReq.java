package com.alipay.zxy.alipayplugin.message.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
@ApiModel(value = "支付宝解约请求model")
public class AlipayUnSignReq implements Serializable {
    @ApiModelProperty(value = "用户签约号")
    private String agreementNo;
}