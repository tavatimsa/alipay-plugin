package com.alipay.zxy.alipayplugin.message.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
public class AlipayUserAgreementSignDTO implements Serializable {
    private String agreementUrl;
}
